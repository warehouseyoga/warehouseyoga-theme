<?php
/*
$grid = array(
	'class'							=> 'c-grid',
	'grid_section_bg'				=> get_field('grid_background_color'),
	'slider_cols'					=> get_sub_field('slider_grid_colums'),
	'image_slider'					=> get_sub_field('image_slider'),
	'slider_image'					=> get_sub_field('slider_image'),
	'slider_image_link'				=> get_sub_field('slider_image_link'),
	'image_cols'					=> get_sub_field('image_grid_colums'),
	'image_row_height'				=> get_sub_field('image_grid_item_height'),
	'image_block_img'				=> get_sub_field('grid_image'),
	'image_block_link'				=> get_sub_field('grid_image_link'),
	'content_grid_cols'				=> get_sub_field('content_grid_colums'),
	'content_block_height'			=> get_sub_field('content_grid_item_height'),
	'image_block_img'				=> get_sub_field('grid_background_color'),
	'grid_content'					=> get_sub_field('grid_content'),
	'grid_content_footer'			=> get_sub_field('grid_content_footer'),
	'background_image'              => get_sub_field('background_image'), 
	'background_color'	            => get_sub_field('background_color'),
	'background_position'           => get_sub_field('background_position'),
	'custom_class'                  => get_sub_field('class_modifier'),
	'text_color'                    => get_sub_field('text_color'),
);
*/

$grid_cols          = get_sub_field('content_grid_colums') * 4;
$grid_item_height   = get_sub_field('content_grid_item_height');
$grid_bg_color      = get_sub_field('grid_background_color');

$block_bg_image = get_sub_field('background_image');

if( is_array($grid['background_image']) ) {        
    $background_image     = wp_get_attachment_image_src($block_bg_image['id'], 'full');
    $background_image_url = $background_image[0];
}
elseif( is_string($block_bg_image) ) {
    $background_image_url = $block_bg_image;
}


?>

<div class="cell <?php echo 'large-' . $grid_cols; ?> small-12 <?= $grid['custom_class'] ?> <?= "text-color-".$grid['text_color'] ?>">
    <div class="c-grid-content-block" style="<?php if ($background_image_url) : echo 'background-image: url('. $background_image_url .');'; endif; ?>
        <?php if ($grid['background_position']) : echo 'background-size: cover; background-position: ' . $grid['background_position'] . ';' ; endif; ?>
        <?php if ($grid['background_color']) : echo 'background-color: ' . $grid['background_color'] . ';' ; endif; ?>">
        <?php
            $grid_content           = get_sub_field('grid_content');
            $grid_content_footer    = get_sub_field('grid_content_footer');
        ?>
        <div class="c-grid-content-block-inner-wrap">
            <?php if( $grid_content ): ?>
                <span class="c-grid-content-block-content">
                    <?= $grid_content ?>
                </span>
            <?php endif; ?>
            <?php if( $grid_content_footer ): ?>
                <span class="c-grid-content-block-footer">
                    <?= $grid_content_footer ?>
                </span>
            <?php endif; ?>    
        </div>
    </div>
</div>

