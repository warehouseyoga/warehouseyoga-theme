<?php

wp_enqueue_script('slick');

/*
$grid = array(
	'class'							=> 'c-grid',
	'grid_section_bg'				=> get_field('grid_background_color'),
	'slider_cols'					=> get_sub_field('slider_grid_colums'),
	'image_slider'					=> get_sub_field('image_slider'),
	'slider_image'					=> get_sub_field('slider_image'),
	'slider_image_link'				=> get_sub_field('slider_image_link'),
	'image_cols'					=> get_sub_field('image_grid_colums'),
	'image_row_height'				=> get_sub_field('image_grid_item_height'),
	'image_block_img'				=> get_sub_field('grid_image'),
	'image_block_link'				=> get_sub_field('grid_image_link'),
	'content_grid_cols'				=> get_sub_field('content_grid_colums'),
	'content_block_height'			=> get_sub_field('content_grid_item_height'),
	'image_block_img'				=> get_sub_field('grid_background_color'),
	'grid_content'					=> get_sub_field('grid_content'),
	'grid_content_footer'			=> get_sub_field('grid_content_footer'),
	'background_image'  => get_sub_field('background_image'), 
	'background_color'	=> get_sub_field('background_color'),
	'background_position' => get_sub_field('background_position'),
	'custom_class'      => get_sub_field('class_modifier'),
	'text_color'        => get_sub_field('text_color'),
);
*/

$slider_cols = get_sub_field('slider_grid_colums') * 4;


$slider_image_url = '';
if( is_array($grid['slider_image']) ) {        
    $slider_image     = wp_get_attachment_image_src($grid['slider_image']['id'], 'full');
    $slider_image_url = $background_image[0];
}
elseif( is_string($grid['slider_image']) ) {
    $slider_image_url = $grid['slider_image'];
}

?>

<div class="cell <?php echo 'large-' . $slider_cols; ?> small-12">
    <div class="c-image-slider">
        <?php if( have_rows('image_slider') ): ?>
            <?php while ( have_rows('image_slider') ) : the_row(); ?>
                <?php
                    $index       = get_row_index();
                    $image_arr   = get_sub_field('slider_image');
                    $image_id    = $image_arr["id"];
                    $image       = wp_get_attachment_image($image_id, 'slider_image');
                    $image_url   = $image ? $image[0] : '';
                    $link        = get_sub_field('slider_image_link');
                    $link_url    = $link['url'];
                    $link_title  = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                    <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                        <div class="c-image-slider_slide">
                            <!-- <span class=""><?= 'slide + ' . $index; ?></span> -->
                            <?= $image; ?>
                        </div>
                    </a>
            <?php endwhile; ?>	
        <?php endif; ?>	
    </div>
</div>

