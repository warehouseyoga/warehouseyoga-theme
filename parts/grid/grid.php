<?php

wp_enqueue_script('jquery-masonry');
wp_enqueue_script('imagesloaded');

/*
$grid = array(
	'class'							=> 'c-grid',
	'grid_section_bg'				=> get_field('grid_background_color'),
	'slider_cols'					=> get_sub_field('slider_grid_colums'),
	'image_slider'					=> get_sub_field('image_slider'),
	'slider_image'					=> get_sub_field('slider_image'),
	'slider_image_link'				=> get_sub_field('slider_image_link'),
	'image_cols'					=> get_sub_field('image_grid_colums'),
	'image_row_height'				=> get_sub_field('image_grid_item_height'),
	'image_block_img'				=> get_sub_field('grid_image'),
	'image_block_link'				=> get_sub_field('grid_image_link'),
	'content_grid_cols'				=> get_sub_field('content_grid_colums'),
	'content_block_height'			=> get_sub_field('content_grid_item_height'),
	'image_block_img'				=> get_sub_field('grid_background_color'),
	'grid_content'					=> get_sub_field('grid_content'),
	'grid_content_footer'			=> get_sub_field('grid_content_footer'),
	'background_image'  => get_sub_field('background_image'), 
	'background_color'	=> get_sub_field('background_color'),
	'background_position' => get_sub_field('background_position'),
	'custom_class'      => get_sub_field('class_modifier'),
	'text_color'        => get_sub_field('text_color'),
);
*/

?>

<?php if( have_rows('grid_components') ): ?>

	<section class="<?= $grid['class'] ?> " >
		
		<div class="grid-container">
			<div class="grid-x grid-margin-x c-grid-masonry">
				<?php while ( have_rows('grid_components') ) : the_row(); ?>
					<!-- <div class="<?php echo 'grid-' . get_row_layout(); ?>"> -->
						<?php if( get_row_layout() == 'grid_slider' ): 
								include get_template_directory() . '/parts/grid/component-image-slider.php';
							endif;
						?>

						<?php if( get_row_layout() == 'grid_image_block' ): 
								include get_template_directory() . '/parts/grid/component-image-block.php';
							endif; 
						?>

						<?php if( get_row_layout() == 'grid_content_block' ): 
								include get_template_directory() . '/parts/grid/component-content-block.php';
							endif;
						?>

					<!-- </div> -->
				<?php endwhile; ?>	
			</div>
		</div>

	</section>	

<?php endif; ?>