<?php
/**
 * The template part for displaying an author byline
 */
?>

<p class="byline">
	<?php
	printf( __( 'Posted: %1$s by %2$s - %3$s', 'whyoga' ),
		get_the_time( __('M j, Y', 'whyoga') ),
		get_the_author_posts_link(),
		get_the_category_list(', ')
	);
	?>
</p>	
