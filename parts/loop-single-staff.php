<?php
/**
 * Template part for displaying a single post
 */
?>

<div class="small-12 large-5 cell">
	<?php the_post_thumbnail('full'); ?>
</div>

<div class="small-12 large-5 cell">
	<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="person" itemscope itemtype="http://schema.org/Person">
							
		<h1 class="entry-title single-title" itemprop="name"><?php the_title(); ?></h1>
		<?= do_shortcode('[social-links]'); ?>						
		<div class="staff-content" itemprop="description">
			<?php if( have_rows('staff_content') ):
				while ( have_rows('staff_content') ) : the_row(); ?>
				<h3 class="staff-content-title">	
					<?php the_sub_field('content_title'); ?>
				</h3>
				<div class="staff-content">	
					<?php the_sub_field('content_text'); ?>
				</div>

				<?php 	endwhile;
					else : // no rows found
				?>
					
			<?php endif; ?>
		</div> <!-- end article section -->
							
														
	</article> <!-- end article -->
</div>	