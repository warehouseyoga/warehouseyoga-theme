<?php
/**
 * Template part for displaying page content in page.php
 */
?>

	<?php whyoga_title_wrapper_start(); ?>
		<h1 class="page-title text-center"><?php the_title(); ?></h1>
	<?php whyoga_title_wrapper_end(); ?>
					
	<?php 
		whyoga_content_wrapper_start();
		the_content();
		whyoga_content_wrapper_end(); ?>
						
						    
					
