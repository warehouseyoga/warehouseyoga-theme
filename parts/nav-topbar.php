<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/responsive-navigation/
 */
?>

<div class="top-bar" id="top-bar-menu">
	<div class="grid-fluid">
		<div class="grid-x align-center align-middle">
			<div class="cell small-9 show-for-746">
				<a href="<?php echo home_url(); ?>">
					<img class="logo" alt="<?php bloginfo('name'); ?>" src="<?= get_template_directory_uri() . '/assets/images/logo_icos_warehouse-yoga.svg'; ?>">
				</a>
			</div>
			<div class="cell small-8 medium-12 show-for-medium">
				<?php joints_top_nav(); ?>	
			</div>
			<div class=" show-for-746">
				<ul class="menu menu-toggle">
					<!-- <li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li> -->
					<li>
						<a data-open="navigationModal">
							<svg style="width: 18px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M328 256c0 39.8-32.2 72-72 72s-72-32.2-72-72 32.2-72 72-72 72 32.2 72 72zm104-72c-39.8 0-72 32.2-72 72s32.2 72 72 72 72-32.2 72-72-32.2-72-72-72zm-352 0c-39.8 0-72 32.2-72 72s32.2 72 72 72 72-32.2 72-72-32.2-72-72-72z"/></svg>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

</div>