<?php
		
/*
	$hero = array(
		'class'             => 'c-hero',
		'fs'				=> get_sub_field('full_screen'),
		'headline'          => get_sub_field('hero_headline'), 
		'add_content'       => get_sub_field('add_hero_content'),
		'content'           => get_sub_field('hero_content'),
		'background_image'  => get_sub_field('background_image'), 
		'background_color'	=> get_sub_field('background_color'),
		'background_position' => get_sub_field('background_position'),
		'custom_class'      => get_sub_field('class_modifier'),
		'text_color'        => get_sub_field('text_color'),
	);  
*/

$background_image_url = '';

if( is_array($hero['background_image']) ) {        
    $background_image     = wp_get_attachment_image_src($hero['background_image']['id'], 'full');
    $background_image_url = $background_image[0];
}
elseif( is_string($hero['background_image']) ) {
    $background_image_url = $hero['background_image'];
}
	

	// echo '<pre>';
	// var_dump($background_image_url);
	// echo '</pre>';	
	$classes = [
		$hero['class'],
		$hero['custom_class'],
		"text-color-".$hero['text_color']
	];
?>

<section class="<?= implode(' ', $classes); ?>">
	<div class="<?php if ($hero['fs']) : echo "fullscreen"; elseif ( 'null' ): echo "standard"; endif; ?>"	
		 style="<?php if ($background_image_url) : echo 'background-image: url('. $background_image_url .');'; endif; ?>
		 <?php if ($hero['background_position']) : echo 'background-size: cover; background-position: ' . $hero['background_position'] . ';' ; endif; ?>
		 <?php if ($hero['background_color']) : echo 'background-color: ' . $hero['background_color'] . ';' ; endif; ?>">
		<div class="grid-container">
			<div class="grid-x align-center align-middle">
				<div class="cell large-8 medium-10 small-12">

					<div class="content">
						<?php if ($hero['headline']): ?><h1 class="page-title text-center"><?= $hero['headline']; ?></h1><?php endif; ?>
						<?php if ($hero['add_content']): ?>
							<div class="hero-content">
								<?= $hero['content']; ?>
							</div>
						<?php endif; ?>
					</div>
								
				</div>	
			</div>
		</div>
	</div>
</section>	