<?php
/**
 * The template part for displaying the navigation modal
 *
 */
?>

<div class="reveal nav-menu" id="navigationModal" data-reveal>
	<?php joints_off_canvas_nav(); ?>

	<?php if ( is_active_sidebar( 'mobile' ) ) : ?>

		<?php dynamic_sidebar( 'mobile' ); ?>

	<?php endif; ?>

	<a class="close-button" data-close aria-label="Close modal" type="button">
		<span aria-hidden="true">&times;</span>
	</a>
</div>
