<?php
/**
 * Template part for displaying event posts
 *
 * Used for single, index, archive, search.
 */

$start_time = the_field('start_time');
$end_time = the_field('end_time');
$event_content = get_field('event_description');
$event_link = get_field('event_signup_link');
$event_staff = get_field('event_staff');
// var_dump($start_time);
// if( $event_link ):
// 	$link_url = $event_link['url'];
// 	$link_title = $event_link['title'];
// 	$link_target = $event_link['target'] ? $event_link['target'] : '_self';
// endif;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">					
	
	<div class="grid-x grid-padding-x">
		<div class="cell small-2 medium-1">
			<div class="">
				<?php echo $start_time; ?>
			</div>
		</div>
		<div class="cell small-10 medium-3">
			<h2 class="event-title">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
					<?php the_title(); ?>
				</a>
            </h2>
            <span class="">
                <?= $event_staff ?>
            </span>
		</div>
		<div class="cell small-12 medium-8">
			<span class="event-content">
				<?php echo $event_content; ?>
			</span>
			<?php if( $event_link ): ?>
				<a class="button" href="<?php echo esc_url($event_link['url']); ?>"><?php echo $event_link['title']; ?></a>
			<?php endif; ?>
		</div>
	</div>	
					
</article> <!-- end article -->