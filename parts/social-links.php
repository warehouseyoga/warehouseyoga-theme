<?php 

// $facebook = get_field('facebook_url', 'option');
// $twitter = get_field('twitter_url', 'option');
// $insta = get_field('insta_url', 'option');
// $youtube = get_field('youtube_url', 'option');
// $spotify = get_field('spotify_url', 'option');

// $links = array(
//     'facebook' => $facebook,
//     'twitter'  => $twitter,
//     'insta'    => $insta,
//     'youtube'  => $youtube,
//     'spotify'  => $spotify,
// );


?>


    <div class="c_social-links">
        <ul class="menu <?php if ( $a['options'] ): echo 'align-center'; endif; ?>">

            <?php foreach( $links as $class => $link ) : ?>
                <?php if ($link): ?>
                <li class="<?= $class; ?>">
                    <a href="<?= $link['url']; ?>" target="_blank" aria-label="Follow us on <?= $link['title']; ?>">
                        <img class="why-svg" src="<?= get_template_directory_uri() . '/assets/images/' . $class . '.svg'; ?>">
                    </a> 
                </li>
                <?php endif; ?>
            <?php endforeach; ?>

        </ul>
    </div>    

