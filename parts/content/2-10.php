<?php 

// 'class'				=> 'c-2-10',
// 'grid_section_bg'	=> get_field('background_color'),
// 'heading'			=> get_sub_field('2_10_heading'),
// 'heading_2'			=> get_sub_field('2_10_heading_2'),
// 'content'			=> get_sub_field('2_10_content'),


/*
$c2_10 = array(
	'class'				=> 'c-2-10',
	'background_image'  => get_sub_field('background_image'), 
	'background_color'	=> get_sub_field('background_color'),
	'background_position' => get_sub_field('background_position'),
	'custom_class'      => get_sub_field('class_modifier'),
	'text_color'        => get_sub_field('text_color'),
	'heading'			=> get_sub_field('2_10_heading'),
	'heading_2'			=> get_sub_field('2_10_heading_2'),
	'content'			=> get_sub_field('2_10_content'),
);
*/

$background_image_url = '';

if( is_array($c2_10['background_image']) ) {        
    $background_image     = wp_get_attachment_image_src($c2_10['background_image']['id'], 'full');
    $background_image_url = $background_image[0];
}
elseif( is_string($c2_10['background_image']) ) {
    $background_image_url = $c2_10['background_image'];
}


?>
    <section class="<?= $c2_10['class'] ?> <?= $c2_10['custom_class'] ?> <?= "text-color-".$c2_10['text_color'] ?>">
      <div style="<?php if ($background_image_url) : echo 'background-image: url('. $background_image_url .');'; endif; ?>
  		 <?php if ($c2_10['background_position']) : echo 'background-size: cover; background-position: ' . $c2_10['background_position'] . ';' ; endif; ?>
  		 <?php if ($c2_10['background_color']) : echo 'background-color: ' . $c2_10['background_color'] . ';' ; endif; ?>">
        <div class="grid-container">
    			<div class="grid-x">
        				<div class="cell medium-2 small-12">
                    <div class="stacked-text">
                        <?php if ($c2_10['heading']): echo '<span class="bold">' . $c2_10['heading'] . '</span>'; endif; ?>
                        <?php if ($c2_10['heading_2']): echo '<span class="thin">' . $c2_10['heading_2'] . '</span>'; endif; ?>
                    </div>
                </div>
                <div class="cell medium-10 small-12">
                    <div class="right-content">
                        <?php if ($c2_10['content']): echo $c2_10['content']; endif; ?>
                    </div>
                </div>
          </div>
        </div>
      </div>
    </section>

<?php 

?>