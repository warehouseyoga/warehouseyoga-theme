<?php 

/*
$content_columns = array(
	'class'					=> 'c-content-columns',
	'background_image'  => get_sub_field('background_image'), 
	'background_color'	=> get_sub_field('background_color'),
	'background_position' => get_sub_field('background_position'),
	'custom_class'      => get_sub_field('class_modifier'),
	'text_color'        => get_sub_field('text_color'),
	'header'				=> get_sub_field('row_header'),
	'alignment'				=> get_sub_field('column_alignment'),
	'center_columns'		=> get_sub_field('center_align_columns'),
);
*/

$background_image_url = '';
	
if( is_array($content_columns['background_image']) ) {        
    $background_image     = wp_get_attachment_image_src($content_columns['background_image']['id'], 'full');
    $background_image_url = $background_image[0];
}
elseif( is_string($content_columns['background_image']) ) {
    $background_image_url = $content_columns['background_image'];
}


?>
<?php $text_align = get_sub_field('text_alignment'); ?>
<?php if( have_rows('columns') ): ?>

    <section class="<?= $content_columns['class'] ?> <?= $content_columns['custom_class'] ?> <?= "text-color-".$content_columns['text_color'] ?>">
      <div style="<?php if ($background_image_url) : echo 'background-image: url('. $background_image_url .');'; endif; ?>
        <?php if ($content_columns['background_position']) : echo 'background-size: cover; background-position: ' . $content_columns['background_position'] . ';' ; endif; ?>
        <?php if ($content_columns['background_color']) : echo 'background-color: ' . $content_columns['background_color'] . ';' ; endif; ?>">
          <div class="grid-container">
      			<div class="grid-x grid-padding-x <?php if ($content_columns['alignment']): echo 'align-' . $content_columns['alignment']; endif; ?> <?php if ($content_columns['center_columns']): echo 'align-center'; endif; ?>">
                <?php if ($content_columns['header']): ?>
                    <div class="cell small-12">
                        <h2><?= $content_columns['header'] ?></h2>
                    </div>
                <?php endif; ?>
                
                <?php while( have_rows('columns') ) : 
                        the_row();
                        $header = get_sub_field('column_header');
                        $content = get_sub_field('column_content');
                        $button = get_sub_field('column_button');
                        $desk_width = get_sub_field('desktop_column_width');
                        $tab_width = get_sub_field('tablet_column_width');
                        $mob_width = get_sub_field('mobile_column_width');
                        ?>
                    
                    <div class="cell 
                        <?php if ($desk_width): echo 'large-' . $desk_width; endif; ?>
                        <?php if ($tab_width): echo 'medium-' . $tab_width; endif; ?>
                        <?php if ($mob_width): echo 'small-' . $mob_width; else: echo 'small-12'; endif; ?>" 
                        <?php if ($text_align): echo 'style="text-align: ' . $text_align . '"'; endif; ?> >

                        <div class="wrap">
                            <?php if ($header): echo '<h3>' . $header . '</h3>'; endif; ?>
                            <?php if ($content): echo '<div class="col-content">' . $content . '</div>'; endif; ?>
                            <?php if ($button): echo '<a class="button hollow" href="' . $button['url'] . '">' . $button['title'] . '</a>'; endif; ?>
                            
                        </div>
                        
                    </div>

                <?php endwhile; ?>
            </div>
          </div>
      </div>
    </section>

<?php 
    endif;
?>