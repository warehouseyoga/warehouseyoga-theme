<?php 

// 'class'				=> 'c-posts-grid',
// 'background_color'	=> get_sub_field('background_color'),
// 'heading_1'			=> get_sub_field('posts_heading_1'),
// 'heading_2'			=> get_sub_field('posts_heading_2'),
// 'type'				=> get_sub_field('posts_grid_type'),
// 'posts'				=> get_sub_field('grid_posts'),
// 'desktop_cols'		=> get_sub_field('desktop_columns'),
// 'tablet_cols'		=> get_sub_field('tablet_columns'),
// 'mobile_cols'		=> get_sub_field('mobile_columns'),




?>

    <section class="<?= $posts_grid['class'] ?>"
        style="<?php if ($posts_grid['background_color']) : echo 'background-color: ' . $posts_grid['background_color'] . ';' ; endif; ?>">
        <div class="grid-container">
            <div class="grid-x">
                <div class="cell medium-2 small-12">
                    <div class="stacked-text">
                        <?php if ($posts_grid['heading_1']): echo '<span class="bold">' . $posts_grid['heading_1'] . '</span>'; endif; ?>
                        <?php if ($posts_grid['heading_2']): echo '<span class="thin">' . $posts_grid['heading_2'] . '</span>'; endif; ?>
                    </div>
                </div>    
            </div>
            <?php 
                $posts = get_sub_field('grid_posts');
                if( $posts ): ?>    
                
                <div class="grid-x grid-padding-x 
                    <?php if($posts_grid['desktop_cols']): echo 'large-up-' . $posts_grid['desktop_cols'] . ' '; endif; ?>
                    <?php if($posts_grid['tablet_cols']): echo 'medium-up-' . $posts_grid['tablet_cols'] . ' '; endif; ?>
                    <?php if($posts_grid['mobile_cols']): echo 'small-up-' . $posts_grid['mobile_cols'] . ' '; endif; ?>
                ">
                
                    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                        <?php setup_postdata($post); ?>
                        <div class="cell c-posts-grid-post <?= $posts_grid['type'] ?>">
                            <?php if ( has_post_thumbnail()) : ?>
                                <a href="<?php the_permalink(); ?>" alt="<?php the_title_attribute(); ?>">
                                    <?php the_post_thumbnail('full'); ?>
                                    <h3 class=""><?php the_title(); ?></h3>
                                </a>
                            <?php endif; ?>
                            <!-- <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> -->
                        </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>  

                </div>
                
            <?php endif; ?>    
        </div>
    </section>

<?php 

?>