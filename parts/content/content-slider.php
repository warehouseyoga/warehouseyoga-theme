<?php 


/*
$content_slider = array(
	'class'					=> 'c-content-slider',
	'background_image'  => get_sub_field('background_image'), 
	'background_color'	=> get_sub_field('background_color'),
	'background_position' => get_sub_field('background_position'),
	'custom_class'      => get_sub_field('class_modifier'),
	'text_color'        => get_sub_field('text_color'),
	'testimonial'			=> get_sub_field('testimonial'),
	'content_slider'		=> get_sub_field('content_slider'),
	'content_slider_color'  => get_sub_field('content_slider_color'),
	'slider_title'			=> get_sub_field('slider_title'),
	'slider_content'		=> get_sub_field('slider_content'),
	'slider_footer'			=> get_sub_field('slider_footer'),
);
*/


wp_enqueue_script('slick');

if( is_array($content_slider['background_image']) ) {        
    $background_image     = wp_get_attachment_image_src($content_slider['background_image']['id'], 'full');
    $background_image_url = $background_image[0];
}
elseif( is_string($content_slider['background_image']) ) {
    $background_image_url = $content_slider['background_image'];
}

?>
    <section class="<?= $content_slider['class'] ?> <?= $content_slider['custom_class'] ?> <?= "text-color-".$content_slider['text_color'] ?> <?php if ($content_slider['testimonial']): echo 'testimonial'; endif; ?>">
      <div style="<?php if ($background_image_url) : echo 'background-image: url('. $background_image_url .');'; endif; ?>
        <?php if ($content_slider['background_position']) : echo 'background-size: cover; background-position: ' . $content_slider['background_position'] . ';' ; endif; ?>
        <?php if ($content_slider['background_color']) : echo 'background-color: ' . $content_slider['background_color'] . ';' ; endif; ?>">
            <div class="grid-container">
                <div class="grid-x align-center align-middle">

                    <div class="cell large-8 medium-10 small-11">
                        <div class="<?= $content_slider['class'] ?>--wrapper">
                            <?php if( have_rows('content_slider') ): ?>
                                <?php while ( have_rows('content_slider') ) : the_row(); ?>
                                    <?php
                                        $index              = get_row_index();
                                        $slider_title       = get_sub_field('slider_title');
                                        $slider_content     = get_sub_field('slider_content');
                                        $slider_footer      = get_sub_field('slider_footer');;
                                    ?>
                                        
                                        <div>    
                                            <?php if ($slider_title): echo '<h4 class="title text-center">' . $slider_title . '</h4>'; endif; ?>
                                            <?php if ($slider_content): echo '<span class="content">' . $slider_content . '</span>'; endif; ?>
                                            <?php if ($slider_footer): echo '<span class="footer">' . $slider_footer . '</span>'; endif; ?>
                                        </div>
                                        
                                <?php endwhile; ?>	
                            <?php endif; ?>	
                        </div>
                    </div>

                </div>
            </div>
      </div>
    </section>

<?php 

?>