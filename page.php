<?php 
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 */

get_header(); ?>
	
	<main id="post-<?php the_ID(); ?>" <?php post_class(''); ?>  role="main" itemscope itemtype="http://schema.org/WebPage">
					
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		    	<?php get_template_part( 'parts/loop', 'page' ); ?>
		    
		  <?php endwhile; endif; ?>							
		    							    
	</main> <!-- end #content -->

<?php get_footer(); ?>