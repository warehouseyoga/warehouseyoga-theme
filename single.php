<?php 
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>
		
<main>		

	<?php whyoga_title_wrapper_start(); ?>

		<h1 class="page-title">
			<?php the_title(); ?>
		</h1>
	
	<?php whyoga_title_wrapper_end(); ?>

					
	<section class="main-content grid-container">		

		<div class="inner-content grid-x grid-margin-x grid-padding-x">

			<div class="small-12 medium-8 large-8 cell" role="main">
			
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
								
					<?php get_template_part( 'parts/loop', 'single' ); ?>
										
				<?php endwhile; else : ?>
							
					<?php get_template_part( 'parts/content', 'missing' ); ?>

				<?php endif; ?>

			</div> <!-- end role="main" -->

			<?php get_sidebar(); ?>

		</div> <!-- end #inner-content -->

	</section>

</main> <!-- end #content -->

<?php get_footer(); ?>