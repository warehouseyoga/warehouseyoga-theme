<?php 
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>
		
<main>		

					
	<section class="main-content grid-container" role="main">		

		<div class="inner-content grid-x grid-margin-x grid-padding-x align-center">
			
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
								
					<?php get_template_part( 'parts/loop', 'single-staff' ); ?>
										
				<?php endwhile; else : ?>
							
					<?php get_template_part( 'parts/content', 'missing' ); ?>

				<?php endif; ?>

		</div> 

	</section> <!-- end role="main" -->

</main> <!-- end #content -->

<?php get_footer(); ?>