<?php 
/**
 * The sidebar containing the main widget area
 */
 ?>

<div id="sidebar1" class="sidebar small-12 medium-3 large-3 cell" role="complementary">

	<?php if ( is_active_sidebar( 'woocommerce' ) ) : ?>
		<?php dynamic_sidebar( 'woocommerce' ); ?>
	<?php endif; ?>

</div>