<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 */

get_header(); ?>

<main>		
	
	<?php whyoga_title_wrapper_start(); ?>

		<h1 class="page-title">
				<?php is_front_page() ? bloginfo('description') : wp_title(''); ?>
		</h1>
	
	<?php whyoga_title_wrapper_end(); ?>

					
	<section class="main-content grid-container">
	
		<div class="inner-content grid-x grid-margin-x grid-padding-x">
	
		    <div class="small-12 medium-8 large-8 cell" role="main">

			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>


					<?php //do_action('whyoga_before_main_content'); ?>
					<!-- To see additional archive styles, visit the /parts directory -->
					<?php get_template_part( 'parts/loop', 'archive' ); ?>
					<?php //do_action('whyoga_after_main_content'); ?>
				    
				<?php endwhile; ?>	

					<?php joints_page_navi(); ?>
					
				<?php else : ?>
											
					<?php get_template_part( 'parts/content', 'missing' ); ?>
						
				<?php endif; ?>
																								
				</div> <!-- end #main -->
		    
		    <?php get_sidebar(); ?>

		</div> <!-- end #inner-content -->

	</section> <!-- end .main-content -->
</main>		
<?php get_footer(); ?>