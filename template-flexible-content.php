<?php
/*
Template Name: Flexible Content (No Sidebar)
*/

get_header(); ?>
			
	<div class="main-content">
					
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<?php 
			
				if( have_rows('flexible_content_sections') ):

				// loop through the rows of data
				while ( have_rows('flexible_content_sections') ) : the_row();

					$layout = get_row_layout();

					switch($layout) {
						case 'hero_layout';
								$hero = array(
									'class'             => 'c-hero',
									'fs'				=> get_sub_field('full_screen'),
									'headline'          => get_sub_field('hero_headline'), 
									'add_content'       => get_sub_field('add_hero_content'),
									'content'           => get_sub_field('hero_content'),
									'background_image'  => get_sub_field('background_image'), 
									'background_color'	=> get_sub_field('background_color'),
									'background_position' => get_sub_field('background_position'),
									'custom_class'      => get_sub_field('class_modifier'),
									'text_color'        => get_sub_field('text_color'),
								);                            
							include get_template_directory() . '/parts/heros/block-hero.php';
						break; 
						case 'grid_layout';
								$grid = array(
									'class'							=> 'c-grid',
									'grid_section_bg'				=> get_field('grid_background_color'),
									'slider_cols'					=> get_sub_field('slider_grid_colums'),
									'image_slider'					=> get_sub_field('image_slider'),
									'slider_image'					=> get_sub_field('slider_image'),
									'slider_image_link'				=> get_sub_field('slider_image_link'),
									'image_cols'					=> get_sub_field('image_grid_colums'),
									'image_row_height'				=> get_sub_field('image_grid_item_height'),
									'image_block_img'				=> get_sub_field('grid_image'),
									'image_block_link'				=> get_sub_field('grid_image_link'),
									'content_grid_cols'				=> get_sub_field('content_grid_colums'),
									'content_block_height'			=> get_sub_field('content_grid_item_height'),
									'image_block_img'				=> get_sub_field('grid_background_color'),
									'grid_content'					=> get_sub_field('grid_content'),
									'grid_content_footer'			=> get_sub_field('grid_content_footer'),
									'background_image'  => get_sub_field('background_image'), 
									'background_color'	=> get_sub_field('background_color'),
									'background_position' => get_sub_field('background_position'),
									'custom_class'      => get_sub_field('class_modifier'),
									'text_color'        => get_sub_field('text_color'),
								);
							include get_template_directory() . '/parts/grid/grid.php';
						break;
						case '2_10_content';
								$c2_10 = array(
									'class'				=> 'c-2-10',
									'background_image'  => get_sub_field('background_image'), 
									'background_color'	=> get_sub_field('background_color'),
									'background_position' => get_sub_field('background_position'),
									'custom_class'      => get_sub_field('class_modifier'),
									'text_color'        => get_sub_field('text_color'),
									'heading'			=> get_sub_field('2_10_heading'),
									'heading_2'			=> get_sub_field('2_10_heading_2'),
									'content'			=> get_sub_field('2_10_content'),
								);
							include get_template_directory() . '/parts/content/2-10.php'; 
						break;
						case 'content_slider';
								$content_slider = array(
									'class'					=> 'c-content-slider',
									'background_image'  => get_sub_field('background_image'), 
									'background_color'	=> get_sub_field('background_color'),
									'background_position' => get_sub_field('background_position'),
									'custom_class'      => get_sub_field('class_modifier'),
									'text_color'        => get_sub_field('text_color'),
									'testimonial'			=> get_sub_field('testimonial'),
									'content_slider'		=> get_sub_field('content_slider'),
									'content_slider_color'  => get_sub_field('content_slider_color'),
									'slider_title'			=> get_sub_field('slider_title'),
									'slider_content'		=> get_sub_field('slider_content'),
									'slider_footer'			=> get_sub_field('slider_footer'),
								);
							include get_template_directory() . '/parts/content/content-slider.php';
						break;
						case 'posts_grid';
								$posts_grid = array(
									'class'					=> 'c-posts-grid',
									'background_color'		=> get_sub_field('background'),
									'heading_1'				=> get_sub_field('posts_heading_1'),
									'heading_2'				=> get_sub_field('posts_heading_2'),
									'type'					=> get_sub_field('posts_grid_type'),
									'posts'					=> get_sub_field('grid_posts'),
									'desktop_cols'			=> get_sub_field('desktop_columns'),
									'tablet_cols'			=> get_sub_field('tablet_columns'),
									'mobile_cols'			=> get_sub_field('mobile_columns'),
								);
							include get_template_directory() . '/parts/content/posts-grid.php';
						break;
						case 'content_columns';
								$content_columns = array(
									'class'					=> 'c-content-columns',
									'background_image'  => get_sub_field('background_image'), 
									'background_color'	=> get_sub_field('background_color'),
									'background_position' => get_sub_field('background_position'),
									'custom_class'      => get_sub_field('class_modifier'),
									'text_color'        => get_sub_field('text_color'),
									'header'				=> get_sub_field('row_header'),
									'alignment'				=> get_sub_field('column_alignment'),
									'center_columns'		=> get_sub_field('center_align_columns'),
								);
							include get_template_directory() . '/parts/content/content-columns.php';
						break;
					}
					
				endwhile;

				else :

				// no layouts found

				endif;

			?>
			
		<?php endwhile; endif; ?>							
	
	</div> <!-- end #content -->

<?php get_footer(); ?>
