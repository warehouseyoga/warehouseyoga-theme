(function($) {

    $(document).ready(function() {
        $(document).foundation();
        // headerMenuInit();
        // heroJumpLinksInit();
        gridSliderInit();
        gridMasonryInit();
        contentSliderInit();
        // splitSliderInit();
        // heroDropdownInit();
        // backToTopInit();
    });
    
    $(window).load(function() {
       // stickyHeaderInit();        
    });
    

    function gridSliderInit() {
        var $sliders = $('.c-image-slider');
        
        if( $sliders.length ) {
            $sliders.on('init', function() {
                setTimeout(function() {
                    // Trigger resize to ensure equalizer doesn't bug out
                    $(window).resize();
                }, 100)
            });
            
            $sliders.slick({
                dots: true,
                arrows: true,
                prevArrow: '<button type="button" class="slick-prev"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path fill="currentColor" d="M238.475 475.535l7.071-7.07c4.686-4.686 4.686-12.284 0-16.971L50.053 256 245.546 60.506c4.686-4.686 4.686-12.284 0-16.971l-7.071-7.07c-4.686-4.686-12.284-4.686-16.97 0L10.454 247.515c-4.686 4.686-4.686 12.284 0 16.971l211.051 211.05c4.686 4.686 12.284 4.686 16.97-.001z" class=""></path></svg></button>',
                nextArrow: '<button type="button" class="slick-next"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path fill="currentColor" d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947 256 10.454 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class=""></path></svg></button>',

                responsive: [
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                        }
                    },
                    {
                        breakpoint: 479,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }                    
                ]
            })
        }
    } 

    function gridMasonryInit() {
        var $gridBlocks = $('.c-grid-masonry');

        if( $gridBlocks.length) {
            $gridBlocks.on('init', function() {
                setTimeout(function() {
                    // Trigger resize to ensure equalizer doesn't bug out
                    $(window).resize();
                }, 100)
            });

            $gridBlocks.masonry({
                // options
                itemSelector: '.cell',
                columnWidth: 200
            });
        }

    }

    function contentSliderInit() {
        var $sliders = $('.c-content-slider--wrapper');
        
        if( $sliders.length ) {
            $sliders.on('init', function() {
                setTimeout(function() {
                    // Trigger resize to ensure equalizer doesn't bug out
                    $(window).resize();
                }, 100)
            });
            
            $sliders.slick({
                dots: true,
                arrows: true,
                prevArrow: '<button type="button" class="slick-prev"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path fill="currentColor" d="M238.475 475.535l7.071-7.07c4.686-4.686 4.686-12.284 0-16.971L50.053 256 245.546 60.506c4.686-4.686 4.686-12.284 0-16.971l-7.071-7.07c-4.686-4.686-12.284-4.686-16.97 0L10.454 247.515c-4.686 4.686-4.686 12.284 0 16.971l211.051 211.05c4.686 4.686 12.284 4.686 16.97-.001z" class=""></path></svg></button>',
                nextArrow: '<button type="button" class="slick-next"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path fill="currentColor" d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947 256 10.454 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class=""></path></svg></button>',
                responsive: [
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 479,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }                    
                ]
            })
        }
    } 
    
// Google Maps API Key: AIzaSyAurkfR_t3skr2ZQybykKR66_-MmUmv6YU


})(jQuery);