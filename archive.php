<?php
/**
 * Displays archive pages if a speicifc template is not set. 
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

get_header(); ?>
			
<main>		

	<?php whyoga_title_wrapper_start(); ?>

		<h1 class="page-title">
				<?php is_front_page() ? bloginfo('description') : wp_title(''); ?><h1 class="page-title"><?php the_archive_title();?>
		</h1>

		<?php the_archive_description('<div class="taxonomy-description">', '</div>');?>

	<?php whyoga_title_wrapper_end(); ?>

					
	<section class="main-content grid-container">

		<div class="content grid-container">
		
			<div class="inner-content grid-x grid-margin-x grid-padding-x">
			
					<div class="small-12 medium-8 large-8 cell" role="main">
									
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						
						<?php do_action('whyoga_before_main_content'); ?>
						<!-- To see additional archive styles, visit the /parts directory -->
						<?php get_template_part( 'parts/loop', 'archive' ); ?>
						<?php do_action('whyoga_after_main_content'); ?>
							
						<?php endwhile; ?>	

							<?php joints_page_navi(); ?>
							
						<?php else : ?>
													
							<?php get_template_part( 'parts/content', 'missing' ); ?>
								
						<?php endif; ?>
			
					</div> <!-- end #main -->
		
				<?php get_sidebar(); ?>
				
				</div> <!-- end #inner-content -->
				
		</div> <!-- end #content -->

	</section>

<?php get_footer(); ?>


