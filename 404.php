<?php
/**
 * The template for displaying 404 (page not found) pages.
 *
 * For more info: https://codex.wordpress.org/Creating_an_Error_404_Page
 */

get_header(); ?>
			
	<div class="content">

		<section class="c-hero" >
			<div class="fullscreen"	>
				<div class="grid-container">
					<div class="grid-x align-center align-middle">
						<div class="cell large-8 medium-10 small-12">

							<div class="content">
								<h1 class="page-title text-center"><?php _e( '404 - Page Not Found', 'warehouseyoga' ); ?></h1>
								<div class="hero-content">
									<p class="text-center">
										<?php _e( 'The article you were looking for was not found, but maybe try looking again!', 'warehouseyoga' ); ?>
									</p>
								</div>
							</div>
										
						</div>	
					</div>
				</div>
			</div>
		</section>

	</div> <!-- end #content -->

<?php get_footer(); ?>