<?php
/**
 * Displays archive pages if a speicifc template is not set. 
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

get_header(); ?>
			
<main>		

<section class="c-hero" >
	<div class="standard" style="background-color: #e2e2e2;">
		<div class="grid-container">
			<div class="grid-x align-center align-middle">
				<div class="cell large-8 medium-10 small-12">

					<div class="content">
						<h1 class="page-title text-center">Events</h1>
					</div>
								
				</div>	
			</div>
		</div>
	</div>
</section>	

					
	<section class="c-events-list main-content grid-container">

		<div class="content grid-container">
		
			<div class="inner-content grid-x grid-padding-x">
			
					<div class="small-12 cell" role="main">
						
						<header class="">
							<?php
								$categories = get_terms(array(
								'taxonomy'   => 'event_cat',
								'hide_empty' => true,
								));
							?>        
							<?php if( $categories ) : ?>
								<nav id="js-eventArchiveFilterNav" class="event-archive_nav">
									<button class="event-archive_filters" data-filter="all">ALL</button>
									<?php foreach( $categories as $category ) : ?>
										<button class="event-archive_filters" data-filter=".term-<?= $category->slug; ?>">
											<?= $category->name; ?>
										</button>
									<?php endforeach; ?>
								</nav>
							<?php endif; ?>
						</header>

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						
						<!-- To see additional archive styles, visit the /parts directory -->
						<?php get_template_part( 'parts/loop', 'event-archive' ); ?>
							
						<?php endwhile; ?>	

							<?php joints_page_navi(); ?>
							
						<?php else : ?>
													
							<?php get_template_part( 'parts/content', 'missing' ); ?>
								
						<?php endif; ?>
			
					</div> <!-- end #main -->
				
				</div> <!-- end #inner-content -->
				
		</div> <!-- end #content -->

	</section>

<?php get_footer(); ?>


