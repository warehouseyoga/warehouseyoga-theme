<?php 
/**
 * The sidebar containing the main widget area
 */
 ?>

<div id="sidebar1" class="sidebar small-12 medium-4 large-4 cell" role="complementary">

	<?php if ( is_active_sidebar( 'blog-sidebar' ) ) : ?>
		<?php dynamic_sidebar( 'blog-sidebar' ); ?>
	<?php endif; ?>

	<?php if ( is_active_sidebar( 'page-sidebar' ) ) : ?>
		<?php dynamic_sidebar( 'page-sidebar' ); ?>
	<?php endif; ?>

	<?php if ( is_active_sidebar( 'woocommerce' ) ) : ?>
		<?php dynamic_sidebar( 'woocommerce' ); ?>
	<?php endif; ?>

</div>