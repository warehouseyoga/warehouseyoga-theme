<?php
/* joints Custom Post Type Example
This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/


// let's create the function for the custom type
function register_staff_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'staff', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Staff Posts', 'whyoga'), /* This is the Title of the Group */
			'singular_name' => __('Staff Member', 'whyoga'), /* This is the individual type */
			'all_items' => __('All Staff Posts', 'whyoga'), /* the all items menu item */
			'add_new' => __('Add New', 'whyoga'), /* The add new menu item */
			'add_new_item' => __('Add New Staff', 'whyoga'), /* Add New Display Title */
			'edit' => __( 'Edit', 'whyoga' ), /* Edit Dialog */
			'edit_item' => __('Edit Staff Post', 'whyoga'), /* Edit Display Title */
			'new_item' => __('New Staff Post', 'whyoga'), /* New Display Title */
			'view_item' => __('View Staff Post', 'whyoga'), /* View Display Title */
			'search_items' => __('Search Staff Posts', 'whyoga'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'whyoga'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'whyoga'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( '', 'whyoga' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-groups', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'staff', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'false', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	register_taxonomy_for_object_type('staff_cat', 'staff');
	/* this adds your post tags to your custom post type */
	register_taxonomy_for_object_type('staff_tag', 'staff');
	
} 

	// adding the function to the Wordpress init
	add_action( 'init', 'register_staff_post_type');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add custom categories (these act like categories)
    register_taxonomy( 'staff_cat', 
    	array('staff'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => true,     /* if this is true, it acts like categories */             
    		'labels' => array(
    			'name' => __( 'Staff Categories', 'whyoga' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Staff Category', 'whyoga' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Staff Categories', 'whyoga' ), /* search title for taxomony */
    			'all_items' => __( 'All Staff Categories', 'whyoga' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Staff Category', 'whyoga' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Staff Category:', 'whyoga' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Staff Category', 'whyoga' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Staff Category', 'whyoga' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Staff Category', 'whyoga' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Staff Category Name', 'whyoga' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true, 
    		'show_ui' => true,
    		'query_var' => true,
    		'rewrite' => array( 'slug' => 'staff-group' ),
    	)
    );   
    
	// now let's add custom tags (these act like categories)
    register_taxonomy( 'staff_tag', 
    	array('staff'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => false,    /* if this is false, it acts like tags */                
    		'labels' => array(
    			'name' => __( 'Staff Tags', 'whyoga' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Staff Tag', 'whyoga' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Staff Tags', 'whyoga' ), /* search title for taxomony */
    			'all_items' => __( 'All Staff Tags', 'whyoga' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Staff Tag', 'whyoga' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Staff Tag:', 'whyoga' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Staff Tag', 'whyoga' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Staff Tag', 'whyoga' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Staff Tag', 'whyoga' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Staff Tag Name', 'whyoga' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true,
    		'show_ui' => true,
    		'query_var' => true,
    	)
    ); 
    
// let's create the function for the custom type
function register_event_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'event', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Event Posts', 'whyoga'), /* This is the Title of the Group */
			'singular_name' => __('Event', 'whyoga'), /* This is the individual type */
			'all_items' => __('All Event Posts', 'whyoga'), /* the all items menu item */
			'add_new' => __('Add New', 'whyoga'), /* The add new menu item */
			'add_new_item' => __('Add New Event', 'whyoga'), /* Add New Display Title */
			'edit' => __( 'Edit', 'whyoga' ), /* Edit Dialog */
			'edit_item' => __('Edit Event', 'whyoga'), /* Edit Display Title */
			'new_item' => __('New Event Post', 'whyoga'), /* New Display Title */
			'view_item' => __('View Event Post', 'whyoga'), /* View Display Title */
			'search_items' => __('Search Event Posts', 'whyoga'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'whyoga'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'whyoga'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( '', 'whyoga' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'show_in_rest' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-calendar-alt', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'event', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'events', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	register_taxonomy_for_object_type('event_cat', 'event');
	/* this adds your post tags to your custom post type */
	register_taxonomy_for_object_type('event_tag', 'event');
	
} 

	// adding the function to the Wordpress init
	add_action( 'init', 'register_event_post_type');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add custom categories (these act like categories)
    register_taxonomy( 'event_cat', 
    	array('event'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => true,     /* if this is true, it acts like categories */             
    		'labels' => array(
    			'name' => __( 'Event Categories', 'whyoga' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Event Category', 'whyoga' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Event Categories', 'whyoga' ), /* search title for taxomony */
    			'all_items' => __( 'All Event Categories', 'whyoga' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Event Category', 'whyoga' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Event Category:', 'whyoga' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Event Category', 'whyoga' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Event Category', 'whyoga' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Event Category', 'whyoga' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Event Category Name', 'whyoga' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true, 
			'show_ui' => true,
			'show_in_rest' => true,
    		'query_var' => true,
    		'rewrite' => array( 'slug' => 'event-group' ),
    	)
    );   
    
	// now let's add custom tags (these act like categories)
    register_taxonomy( 'event_tag', 
    	array('event'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => false,    /* if this is false, it acts like tags */                
    		'labels' => array(
    			'name' => __( 'Event Tags', 'whyoga' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Event Tag', 'whyoga' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Event Tags', 'whyoga' ), /* search title for taxomony */
    			'all_items' => __( 'All Event Tags', 'whyoga' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Event Tag', 'whyoga' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Event Tag:', 'whyoga' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Event Tag', 'whyoga' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Event Tag', 'whyoga' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Event Tag', 'whyoga' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Event Tag Name', 'whyoga' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true,
    		'show_ui' => true,
			'query_var' => true,
			'show_in_rest' => true,
    	)
    ); 
