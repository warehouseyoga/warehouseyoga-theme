<?php
/**
 * 
 * This file is for templating mods for the whyoga starter theme, woocommerce mods (may want it's own file if things get out of hand)
 * 
 */


remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

function whyoga_content_wrapper_start() {
    echo '<section class="main-content">
            <div class="grid-container">
                <div class="grid-x align-center align-middle">
                    <div class="cell small-12">';
}

function whyoga_content_wrapper_end() {
    echo '          </div>
                </div>
            </div>
        </section>';
}

function whyoga_main_wrapper_start() {
    echo '<main>';
}

function whyoga_main_wrapper_end() {
    echo '</main>';
}

function whyoga_woo_main_wrapper_start() {
    echo '<main>
            <section class="main-content">
                <div class="grid-container">
                    <div class="grid-x">';
}

function whyoga_woo_main_wrapper_end() {
    echo '          </div>
                </div>
            </section>
        </main>';
}

function whyoga_woo_content_wrapper_start() {
    echo '<div class="cell small-12">';
}

function whyoga_woo_content_wrapper_end() {
    echo '          </div>';
}

function whyoga_woo_sidebar_start() {
    echo '<div class="cell small-12 medium-3">';
}

function whyoga_woo_sidebar_end() {
    echo '</div>';
}

// function whyoga_woo_get_sidebar() {
//     get_sidebar('woocommerce');
// }

add_action('whyoga_before_main_content', 'whyoga_content_wrapper_start', 10);
add_action('whyoga_after_main_content', 'whyoga_content_wrapper_end', 10);
add_action('woocommerce_before_main_content', 'whyoga_woo_main_wrapper_start', 9);
add_action('woocommerce_after_main_content', 'whyoga_woo_main_wrapper_end', 11);
add_action('woocommerce_before_main_content', 'whyoga_woo_content_wrapper_start', 11);
add_action('woocommerce_after_main_content', 'whyoga_woo_content_wrapper_end', 10);
// add_action('woocommerce_before_main_content', 'whyoga_woo_get_sidebar', 10);
// add_action('woocommerce_sidebar', 'whyoga_woo_sidebar_start', 9 );
// add_action('woocommerce_sidebar', 'whyoga_woo_sidebar_end', 11 );

function whyoga_title_wrapper_start() {
    echo '<section class="c-title-wrapper">
            <div class="grid-container">
                <div class="grid-x align-center align-middle">
                    <div class="cell small-12 text-center">';
}

function whyoga_title_wrapper_end() {
    echo '          </div>
                </div>
            </div>
        </section>';
}


if ( is_product() ){
    remove_action( 'woocommerce_before_main_content', 'whyoga_woo_get_sidebar', 10 );
}

function whyoga_add_to_cart_text_filter( $text, $product ){
    return 'Buy Now';
}
add_filter( 'woocommerce_product_add_to_cart_text', 'whyoga_add_to_cart_text_filter', 10, 2 );

// Remove the product description Title
add_filter( 'woocommerce_product_description_heading', 'whyoga_remove_product_description_heading' );
function whyoga_remove_product_description_heading() {
 return '';
}

// Change the product description title

function whyoga_change_product_description_heading() {
 return __('NEW TITLE HERE', 'woocommerce');
}


?>