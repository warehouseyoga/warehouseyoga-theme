<?php
// SIDEBARS AND WIDGETIZED AREAS
function whyoga_register_sidebars() {
	register_sidebar(array(
		'id' => 'page-sidebar',
		'name' => __('Page Sidebar', 'whyoga'),
		'description' => __('The sidebar seen when using the sidebar page template.', 'whyoga'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar(array(
		'id' => 'mobile',
		'name' => __('Mobile', 'whyoga'),
		'description' => __('The mobile sidebar.', 'whyoga'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar(array(
		'id' => 'woocommerce',
		'name' => __('WooCommerce', 'whyoga'),
		'description' => __('The WooCommerce sidebar.', 'whyoga'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar(array(
		'id' => 'blog-sidebar',
		'name' => __('Blog Sidebar', 'whyoga'),
		'description' => __('The sidebar seen on the blog.', 'whyoga'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

} /* end register sidebars */

add_action( 'widgets_init', 'whyoga_register_sidebars' );