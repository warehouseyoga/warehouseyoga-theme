<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
        

    // Adding scripts file in the footer
    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/scripts/scripts.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true );
   
    // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/styles/style.css', array(), filemtime(get_template_directory() . '/assets/styles/scss'), 'all' );

    // wp_enqueue_style( 'fas-solid', get_template_directory_uri() . '/assets/styles/fontawesome-free/solid.css', array(), filemtime(get_template_directory() . '/assets/styles/scss/fontawesome-free'), 'all' );
    // wp_enqueue_style( 'fas-fas', get_template_directory_uri() . '/assets/styles/fontawesome-free/fontawesome.css', array(), filemtime(get_template_directory() . '/assets/styles/scss/fontawesome-free'), 'all' );

    // Register Adobe Web Font Kit
    wp_enqueue_style( 'adobe-fonts', 'https://use.typekit.net/jky8ity.css', array(), '', 'all' );
    
     // Register temporary stylesheet
    wp_enqueue_style( 'site-temp-css', get_template_directory_uri() . '/style.css', array(), '', 'all' );

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);