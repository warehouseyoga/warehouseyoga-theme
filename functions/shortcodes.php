<?php

add_shortcode('social-links', function($atts) {
    $a = shortcode_atts(array(
        'options' => false
    ), $atts);

    if( $a['options'] ) {
        $links = array(
            'facebook' => get_field('facebook_url', 'option'),
            'twitter'  => get_field('twitter_url', 'option'),
            'instagram'=> get_field('insta_url', 'option'),
            'youtube'  => get_field('youtube_url', 'option'),
            'spotify'  => get_field('spotify_url', 'option'),  
            'shop'     => get_field('shop_url', 'option'),            
        );
    }
    else {
        $links = array(
            'facebook'  => get_field('facebook_url'),
            'twitter'   => get_field('twitter_url'),
            'instagram' => get_field('insta_url'),
            'youtube'   => get_field('youtube_url'),
            'spotify'   => get_field('spotify_url'),
            'shop'      => get_field('shop_url'),
        );
    }

    ob_start();
        include get_template_directory() . '/parts/social-links.php';

    return ob_get_clean();
});