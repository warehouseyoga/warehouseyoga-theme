<?php
/**
 * The template for displaying the footer. 
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */		

	$white_logo =  get_field('white_logo', 'option');

 ?>
					
				<footer class="footer" role="contentinfo">

					<div class="grid-container">

						<div class="inner-footer grid-x align-center align-middle">
							
							<div class="small-12 medium-10 large-4 contact cell">
								<h3 class="location-name">warehouse yoga</h3>
								<p class="location-address">
									<?= get_field('address_1', 'option'); ?><br>
									<?= get_field('address_2', 'option'); ?><br>
									<?= get_field('city', 'option'); ?>, <?= get_field('state', 'option'); ?> <?= get_field('zip_code', 'option'); ?>
								</p>
								<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
							</div>

							<div class="small-12 medium-10 large-4 logo cell">
								
								<a href="<?php echo home_url(); ?>">
									<img src="<?= $white_logo['url']; ?>">
								</a>
								<nav role="footer-navigation">
									<?php 

										$get_social = get_field('show_footer_social', 'option');
										if ($get_social):

									// 	$facebook = get_field('facebook_url', 'option');
									// 	$twitter = get_field('twitter_url', 'option');
									// 	$insta = get_field('insta_url', 'option');
									// 	$youtube = get_field('youtube_url', 'option');
									// 	$spotify = get_field('spotify_url', 'option');

									// 	$links = array(
									// 	    'facebook' => $facebook,
									// 	    'twitter'  => $twitter,
									// 	    'insta'    => $insta,
									// 	    'youtube'  => $youtube,
									// 	    'spotify'  => $spotify,
									// 	);

									// include get_template_directory() . '/parts/social-links.php';

										echo do_shortcode('[social-links options="true"]');
										endif;
									?>
								</nav>
							</div>
							
							<div class="small-12 medium-10 large-4 newsletter cell">
								<?php the_field('footer_right_column', 'option'); ?>
							</div>
							
						</div> <!-- end #inner-footer -->

					</div>
									
				</footer> <!-- end .footer -->
			
			</div>  <!-- end .off-canvas-content -->
					
		</div> <!-- end .off-canvas-wrapper -->
		
		<?php wp_footer(); ?>
		
	</body>
	
</html> <!-- end page -->